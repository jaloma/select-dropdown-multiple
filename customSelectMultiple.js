/**
 * Create an object of all attributes for a HTML Element
 * @param {HTMLElement} el - an HTML element
 * @returns {Object} - attributes for a HTML Element
 */
function getAttributes(el) {
	const attributes = {};

	for (let i = 0; i < el.attributes.length; i++) {
		let attr = el.attributes[i];
		attributes[attr.name] = attr.value;
	}

	return attributes;
}

class CustomSelectMultiple {
	/**
	 * Constructor for Custom Select Element
	 * @param {HTMLElement} selectMultipleElement - selectMultipleElement
	 */
	constructor(selectMultipleElement) {
		this.selectElement = selectMultipleElement;
		this.options = selectMultipleElement.querySelectorAll('option');
		this.customSelectElement = this._getCustomSelectElement();
		this.customOptions = this._getCustomOptionsElement();
		this.customSelectedOptions = [];

		// Init
		const container = this.selectElement.parentElement;
		this.selectElement.style.display = 'none';
		container.appendChild(this.customSelectElement);
		container.appendChild(this.customOptions);
		this._calculateCustomOptionPosition();

		// Bind Events
		document.addEventListener('scroll', () => {
			this._calculateCustomOptionPosition();
		});

		document.addEventListener('click', (e) => {
			if (this.customSelectElement.classList.contains('show') && !e.target.classList.contains('multiple-select')) {
				this.customSelectElement.classList.remove('show');
			}
		});
	}

	_getCustomSelectElement() {
		const customElement = document.createElement('button');
		const selectAttributes = getAttributes(this.selectElement);
		Object.keys(selectAttributes).forEach(key => {
			customElement.setAttribute(key, selectAttributes[key]);
		});

		customElement.classList.add('multiple-select', 'multiple-select-btn', 'btn', 'btn-primary');
		customElement.setAttribute('type', 'button');

		customElement.textContent = this._getDefaultOptionText();

		customElement.addEventListener('click', () => {
			customElement.classList.toggle('show');
		});

		return customElement;
	}

	_getCustomOptionsElement() {
		const optionsElement = document.createElement('div');
		optionsElement.classList.add('multiple-select', 'multiple-select-options-container');

		this.options.forEach(option => {
			if (!!option.attributes.value && !!option.attributes.value.value) {
				const optionElement = document.createElement('div');
				optionElement.classList.add('multiple-select', 'multiple-select-option');

				const optionsAttributes = getAttributes(option);
				Object.keys(optionsAttributes)
					.filter(key => key === 'value')
					.forEach(key => {
						optionElement.setAttribute(key, optionsAttributes[key]);
					});
				optionElement.textContent = option.textContent.toUpperCase();

				optionsElement.appendChild(optionElement);

				optionElement.addEventListener('click', (e) => {
					this._toggleOption({
						value: e.target.attributes.value.value,
						text: e.target.textContent
					});
				});
			}
		});

		return optionsElement;
	}

	_getDefaultOptionText() {
		return Array.from(this.options).find(option => !option.attributes.value.value).textContent.toUpperCase();
	}

	_toggleOption(option) {
		const existantValueIndex = this.customSelectedOptions.findIndex(selectedOption => selectedOption.value === option.value);
		if (existantValueIndex > -1) {
			this.customSelectedOptions.splice(existantValueIndex, 1);
		} else {
			this.customSelectedOptions.push(option);
		}
		this._updateSelectedOption();
	}

	_updateSelectedOption() {
		this.options.forEach(option => {
			if (this.customSelectedOptions.find(selectedOption => selectedOption.value === option.attributes.value.value)) {
				option.setAttribute('selected', 'selected');
			} else {
				option.removeAttribute('selected');
			}
		});

		this.selectElement.dispatchEvent(new Event('change'));

		this.customOptions.querySelectorAll('.multiple-select-option').forEach(option => {
			if (this.customSelectedOptions.find(selectedOption => selectedOption.value === option.attributes.value.value)) {
				option.classList.add('selected');
			} else {
				option.classList.remove('selected');
			}
		});

		this.customSelectElement.textContent = this.customSelectedOptions.length === 0 ? this._getDefaultOptionText() : this.customSelectedOptions.map(selectOption => selectOption.text).join(', ');
	}

	_calculateCustomOptionPosition() {
		const customSelectClientRect = this.customSelectElement.getBoundingClientRect();
		this.customOptions.style.top = customSelectClientRect.top + customSelectClientRect.height + 'px';
		this.customOptions.style.width = customSelectClientRect.width + 'px';
	}
}

module.exports = CustomSelectMultiple;
